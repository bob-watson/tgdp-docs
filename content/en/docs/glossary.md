---
title: Chronologue Glossary
draft: true
menu:
  docs:
    parent: "docs"
    identifier: "Glossary"
weight: 800
---
Explains terminology used in this project