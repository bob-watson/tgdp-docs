---
title : "The Chronologue"
description: "The Chronologue is a fictional tool built to test and demonstrate The Good Docs Project templates."
lead: "The Chronologue is a fictional tool built to test and demonstrate The Good Docs Project templates."
date: 2022-11-07T08:47:36+00:00
lastmod: 2022-11-07T08:47:36+00:00
draft: false
images: []
menu:
  docs:
    identifier: "docs"
---
