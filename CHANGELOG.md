### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

#### v0.1 2022-11-07

Initialized the repository with the Chronologue files that need to be populated.
